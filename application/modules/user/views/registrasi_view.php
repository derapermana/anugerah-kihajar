<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.2.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Metronic | Form Layouts</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/select2/select2.css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/css/plugins.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-datepicker/css/datepicker.css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
    <!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
    <body>
        <!-- BEGIN HEADER -->
        <div class="page-header" style="height:75px !important;">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html"><img src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout3/img/logo-default.png" alt="logo" class="logo-default"></a>
                    </div>
                    <!-- END LOGO -->
                </div>
            </div>
            <!-- END HEADER TOP -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN PAGE CONTAINER -->
        <div class="page-container">
            <!-- BEGIN PAGE CONTENT -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="row">
                        <div class="portlet box blue col-md-12">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-user"></i> Form Registrasi
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <?= form_open_multipart('user/registrasi', 'class="form-horizontal"'); ?>
                                <?= validation_errors(); ?>
                                <?php if (isset($error)): ?>
                                    <div class="alert alert-danger"><?= $error; ?></div>
                                <?php endif; ?>
                                <div class="form-body">
                                    <h3 class="form-section">Identitas Pribadi Pengusul</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Nama Lengkap</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="nama_user" placeholder="Nama Lengkap" value="<?= set_value('nama_user'); ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Jenis Kelamin</label>
                                                <div class="col-md-9">
                                                    <select name="jenis_kelamin" class="select2me form-control">
                                                        <option value="L">Laki-laki</option>
                                                        <option value="P">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Tempat Lahir</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" value="<?= set_value('tempat_lahir'); ?>"  required>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Tanggal Lahir</label>
                                                <div class="col-md-4">
                                                    <div class="input-group date date-picker" data-date-format="yyyy/mm/dd">
                                                        <input type="text" class="form-control tanggal" name="tanggal_lahir" value="<?= set_value('tanggal_lahir'); ?>" required="" data-date-format="yyyy/mm/dd">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                    <!-- /input-group -->
                                                    <span class="help-block">
                                                        Pilih Tanggal </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email</label>
                                                <div class="col-md-9">
                                                    <input type="email" class="form-control" name="email_user" placeholder="Alamat Email" value="<?= set_value('email_user'); ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">No Handphone</label>
                                                <div class="col-md-9">
                                                    <input type="tel" class="form-control" name="no_hp_user" placeholder="+62xxxxxxxx" value="<?= set_value('no_hp_user'); ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Jabatan</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="jabatan" placeholder="Jabatan" value="<?= set_value('jabatan'); ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Instansi</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="instansi" placeholder="Instansi" value="<?= set_value('instansi'); ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Alamat Instansi</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="alamat_kantor" placeholder="Alamat Instasi" value="<?= set_value('alamat_kantor'); ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Surat Tugas/ Surat Keterangan</label>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" name="surat" placeholder="Surat Tugas/ Surat Keterangan"  required>
                                                    <span class="help-block">Jenis File: pdf,jpg,jpeg,png. Maks:1 MB</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Password</label>
                                                <div class="col-md-9">
                                                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                                                    <span class="help-block">Minimal 6 Karakter</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Konfirmasi Password</label>
                                                <div class="col-md-9">
                                                    <input type="password" class="form-control" name="konf_password" placeholder="Konfirmasi Password" required>
                                                    <span class="help-block">Harus sama dengan password</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="submit" name="submit" class="btn green" value="Registrasi">
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        <!-- BEGIN PRE-FOOTER -->
        <!--        <div class="page-prefooter">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                <h2>About</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam dolore.
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                                <h2>Subscribe Email</h2>
                                <div class="subscribe-form">
                                    <form action="javascript:;">
                                        <div class="input-group">
                                            <input type="text" placeholder="mail@email.com" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn" type="submit">Submit</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                <h2>Follow Us On</h2>
                                <ul class="social-icons">
                                    <li>
                                        <a href="javascript:;" data-original-title="rss" class="rss"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="youtube" class="youtube"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                <h2>Contacts</h2>
                                <address class="margin-bottom-40">
                                    Phone: 800 123 3456<br>
                                    Email: <a href="mailto:info@metronic.com">info@metronic.com</a>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>-->
        <!-- END PRE-FOOTER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="container">
                2016 &copy; Pustekkom. Kemdikbud.
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/respond.min.js"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/select2/select2.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout3/scripts/demo.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/scripts/form-samples.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Demo.init(); // init demo features
                FormSamples.init();
                $('.tanggal').datepicker();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>