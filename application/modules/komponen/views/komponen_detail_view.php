<link rel="stylesheet" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>plugins/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css">
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/scripts/ui-general.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>plugins/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-bootpag/jquery.bootpag.min.js" type="text/javascript"></script>



<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo (isset($level_2) ? $level_2 : $current_page); ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="note note-info">
                    <h3>
                        <?= $komponen->komponen; ?>
                    </h3>
                    <div class="input-group col-md-3">
                        <?= $komponen->form; ?><?php if ($komponen->class != 'check'): ?><span class="input-group-addon"><?= $komponen->label; ?></span><?php
                        endif;

                        echo $komponen->form2;
                        ?>
                    </div>
                    <br />
                </div>
                <?php
                $success_msg = $this->session->flashdata('success_msg');
                $error_msg = $this->session->flashdata('error_msg');
                ?>
                <div class="alert alert-success alert-dismissable" style="<?= empty($success_msg) ? 'display:none' : '' ?>">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?= $success_msg ?>
                </div>
                <div class="alert alert-danger alert-dismissable" style="<?= empty($error_msg) ? 'display:none' : '' ?>" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?= $error_msg ?>
                </div>

                <h4 class="block">File/ dokumen <a class="btn default blue" data-toggle="modal" href="#basic">Tambah File </a></h4>
                <div class="row">
                    <?php if (isset($data)): ?>
                        <?php foreach ($data as $x): ?>
                            <div class="col-sm-12 col-md-3">
                                <div class="thumbnail">
                                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzNjUiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMzY1IiBoZWlnaHQ9IjIwMCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjE4Mi41IiB5PSIxMDAiIHN0eWxlPSJmaWxsOiNhYWE7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LXNpemU6MjNweDtmb250LWZhbWlseTpBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjtkb21pbmFudC1iYXNlbGluZTpjZW50cmFsIj4zNjV4MjAwPC90ZXh0Pjwvc3ZnPg==" alt="100%x200" style="width: 100%; height: 200px; display: block;" data-src="holder.js/100%x200">
                                    <div class="caption">
                                        <p>Nama File: <?= $x->file_name; ?></p>
                                        <p>Diupload Tanggal: <?= $x->created_at; ?></p>
                                        <p>
                                            <a href="<?= $this->config->base_url(); ?>data/delete_file/<?= $x->id_data; ?>" class="btn red">
                                                Hapus </a>
                                            <a href="<?= $x->path_data . $x->file_name; ?>" class="btn green">
                                                Download </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal Title</h4>
            </div>
            <?= form_open_multipart('data/upload', 'class="form-horizontal"'); ?>
            <?= validation_errors(); ?>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" name="id_jawaban" value="<?= $jawaban->id_jawaban; ?>">
                    <input type="file" name="userfile" class="btn btn-blue">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <input type="submit" name="submit" class="btn blue" value="Upload">
            </div>
            <?= form_close(); ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    jQuery(document).ready(function () {
        $(".check").bootstrapSwitch({
            onText: 'Ya',
            offText: 'Tidak',
            onColor: 'success',
            offColor: 'danger'
        });
        UIGeneral.init();
    });
</script>