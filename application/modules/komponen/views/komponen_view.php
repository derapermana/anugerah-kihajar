<link rel="stylesheet" type="text/css" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-toastr/toastr.min.css"/
      <link rel="stylesheet" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>plugins/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css">
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/scripts/ui-general.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>plugins/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-bootpag/jquery.bootpag.min.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo (isset($level_2) ? $level_2 : $current_page); ?>
                    <?php // print_r($kat_komp); ?>
                </div>
            </div>
            <?php
            $success_msg = $this->session->flashdata('success_msg');
            $error_msg = $this->session->flashdata('error_msg');
            ?>
            <div class="alert alert-success alert-dismissable" style="<?= empty($success_msg) ? 'display:none' : '' ?>">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                <?= $success_msg ?>
            </div>
            <div class="alert alert-danger alert-dismissable" style="<?= empty($error_msg) ? 'display:none' : '' ?>" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <?= $error_msg ?>
            </div>
            <div class="portlet-body">
                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer" id="table_komponen" role="grid" aria-describedby="sample_1_info">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Pertanyaan</th>
                                <th></th>
                                <th></th>
                                <!--<th></th>-->
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <center>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Jawab Instrumen</h4>
                </div>
                <form method="post" action="komponen/jawab" class="form-horizontal" id="formJawab">
                    <?= validation_errors(); ?>
                    <div class="modal-body">
                        <div class="form-group">

                            <div class="input-group col-md-6" id="form_jawab">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <input type="submit" id="submitjawab" name="submit" class="btn blue" value="Simpan">
                    </div>
                </form>
            </center>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    $(document).ready(function () {
        var table = $('#table_komponen').DataTable({
            "ajax": "komponen/list_komponen/<?= $kat_komp->id_kat_komp; ?>",
//            responsive: "true",
            "columnDefs": [{
                    "targets": 0,
                    "visible": false,
                }],
        });

        var dataupdate = 0;
        $('#table_komponen tbody').on('click', '.btn-jawab', function () {
            // dataupdate = $(this).parents('tr').find("td").html();
            dataupdate = table.row($(this).parents('tr')).data();

//            $("#ednip").val(dataupdate[0]);
            var id_komponen = dataupdate[0];
//            console.log(provinsi);
            $.ajax({
                type: "POST",
                url: "<?= $this->config->base_url('komponen/get_form'); ?>",
                data: "id_komponen=" + id_komponen,
                success: function (data) {
                    $("#form_jawab").html(data);
                    $("#ajax-loading").modal('hide');
                    $(".check").bootstrapSwitch({
                        onText: 'Ya',
                        offText: 'Tidak',
                        onColor: 'success',
                        offColor: 'danger'
                    });
                }
            });
        });

        $("#submitjawab").click(function () {
            $("#formJawab").submit();
        });
        //callback handler for form submit
        $("#formJawab").submit(function (e) {
            var postData = $(this).serializeArray();
//            console.log('serializeArray : ' + $(this).serializeArray());
            $.ajax({
                url: "komponen/jawab",
                type: "POST",
                data: postData,
                beforeSend: function () {
                    // toastInfoProgress();
                    $("#submitjawab").attr('disabled', true);
                    $("#submitjawab").html('Loading...');

                },
                success: function (res, textStatus, jqXHR) {
                    if (res.iserror == 100) {
                        toastr.success(res.errmsg);
                        $("#formJawab").trigger('reset');
                        $(".close").click();
                    } else {
                        toastr.error(res.errmsg);
                    }
                    table.ajax.reload(null, false);
                    console.log('Result : ' + res);
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    toastr.error('Gagal tambah data', err.Message);
                    console.log('Error : ' + xhr + '|' + status + '|' + error);
                },
                complete: function () {
                    $("#submitjawab").attr('disabled', false);
                    $("#submitjawab").html('Tambah');
                }
            });
            e.preventDefault(); //STOP default action
            //e.unbind(); //unbind. to stop multiple form submit.
        });

    });
</script>