<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start <?php echo ( isset($current_page) && $current_page == 'dashboard' ? 'active' : ''); ?> ">
                    <a href="<?= site_url('dashboard'); ?>">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item <?php echo ( isset($current_page) && $current_page == 'peserta' ? 'active' : ''); ?> ">
                    <a href="<?= site_url('peserta/detail'); ?>">
                        <i class="icon-user-follow"></i>
                        <span class="title">Profil Calon Penerima Anugerah</span>
                    </a>
                </li>
                <li class="nav-item <?php echo ( isset($current_page) && $current_page == 'komponen' ? 'active' : ''); ?> ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Instrumen</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php foreach ($kategori_komponen as $x):?>
                        <li class="nav-item  ">
                            <a href="<?= site_url('komponen?id='.$x->id_kat_komp); ?>" class="nav-link ">
                                <span class="title"><?=$x->nama_kat_komp;?></span>
                            </a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><?=$current_page;?>
                        <?php echo (isset($level_2)?'<small>'.$level_2.'</small>':'');?>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="#"><?=$current_page;?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <?php if(isset($level_2)):?>
                <li>
                    <a href="#"><?=$level_2;?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <?php endif;?>
                <?php if(isset($level_3)):?>
                <li>
                    <a href="#"><?=$level_3;?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <?php endif;?>
            </ul>
            <!-- END PAGE BREADCRUMB -->
