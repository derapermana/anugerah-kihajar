<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends MY_Model {

    static $table = "user";

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        $this->db->where('password', sha1($this->config->item('encryption_key') . $this->input->post('password')));
        $this->db->where('email_user', $this->input->post('email'));
        $this->db->where('aktif', '1');
        $query = $this->db->get(static::$table);
        return $query->row(0, get_class($this));
    }

}
