<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <?php (isset($level_2) ? $level_2 : $current_page); ?>
                </div>
            </div>
            <div class="portlet-body">
                <form role="form" action="<?= base_url(); ?>Peserta/post_add_peserta" method="post">
                    <?= validation_errors(); ?>
                    <div class="form-group">
                        <label for="kategori">Kategori</label>
                        <select name="kategori" id="kategori" class="form-control">
                            <option value="">Pilih Kategori</option>
                            <?php foreach ($kat_peserta as $x): ?>
                                <option value="<?= $x->id_kat_peserta; ?>"><?= $x->kategori; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id_provinsi">Provinsi</label>
                        <select name="id_provinsi" id="id_provinsi" class="form-control">
                            <option value="">Pilih Provinsi</option>
                            <?php foreach ($kode_prov as $x): ?>
                                <option value="<?= $x->kode_wilayah; ?>"><?= $x->nama; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group" id="kota">
                        <label for="id_kota">Kota/ Kabupaten</label>
                        <select name="id_kota" id="id_kota" class="form-control">
                            <option value="0">Pilih Provinsi terlebih dahulu</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama_peserta">Nama Lengkap Peserta</label>
                        <input type="text" name="nama_peserta" id="nama_peserta" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email_peserta">Email Peserta</label>
                        <input type="email" name="email_peserta" id="email_peserta" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label>
                        <input type="text" name="jabatan" id="jabatan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="no_hp_peserta">No Handpone Peserta</label>
                        <input type="text" name="no_hp_peserta" id="no_hp_peserta" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="alamat_kantor">Alamat Kantor</label>
                        <input type="text" name="alamat_kantor" id="alamat_kantor" class="form-control">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary col-md-2 col-md-offset-6">Daftarkan</button>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<script>
    jQuery(document).ready(function () {
        $("#kategori").change(function () {
            if ($("#kategori").val() == 1) {
                $("#kota").children().prop('disabled', true);
            } else {
                $("#kota").children().prop('disabled', false);
            }
        });
        $("#id_provinsi").change(function () {
            $("#ajax-loading").modal('show');
            var provinsi = $("#id_provinsi").val();
            $.ajax({
                type: "POST",
                url: "<?= $this->config->base_url('peserta/get_kota'); ?>",
                data: "id_provinsi=" + provinsi,
                success: function (data) {
                    $("#id_kota").html(data);
                    $("#ajax-loading").modal('hide');
                }
            });
        });
    });
</script>