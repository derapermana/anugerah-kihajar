<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public $data = array('current_page' => "dashboard");

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
//        $this->load->model('User_model');
    }

    public function index() {
        $data = $this->data;
        $this->render('dashboard_view',$data);
    }

}

?>