<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <?php (isset($level_2)?$level_2:$current_page);?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="sample_editable_1_new" class="btn green">
                                    Add New <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer"><div class="row"><div class="col-md-6 col-sm-12"><div class="dataTables_length" id="sample_1_length"><label>  <select name="sample_1_length" aria-controls="sample_1" class="form-control input-xsmall input-inline"><option value="5">5</option><option value="15">15</option><option value="20">20</option><option value="-1">All</option></select> records</label></div></div><div class="col-md-6 col-sm-12"><div id="sample_1_filter" class="dataTables_filter"><label>My search: <input type="search" class="form-control input-small input-inline" placeholder="" aria-controls="sample_1"></label></div></div></div><div class="table-scrollable"><table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid" aria-describedby="sample_1_info">
                            <thead>
                                <tr role="row"><th class="table-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label="

                                                   " style="width: 24px;">
                                        <div class="checker"><span><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"></span></div>
                                    </th><th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                             Username
                                             : activate to sort column ascending" style="width: 286px;">
                                        Username
                                    </th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="
                                             Email
                                             " style="width: 471px;">
                                        Email
                                    </th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="
                                             Points
                                             " style="width: 177px;">
                                        Points
                                    </th><th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="
                                             Joined
                                             : activate to sort column ascending" style="width: 257px;">
                                        Joined
                                    </th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="
                                             Status
                                             " style="width: 273px;">
                                        Status
                                    </th></tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>