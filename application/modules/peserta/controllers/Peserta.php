<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Peserta extends MY_Controller {

    public $data = array('current_page' => "peserta");

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Peserta_model');
        $this->load->model('data/Data_model');
        $this->load->model('komponen/Kat_komp_model');
        $this->load->model('Kode_wilayah_model');
        $this->load->model('komponen/Jawaban_model');
        $this->load->model('komponen/Komponen_model');
    }

    public function manajemen_peserta() {
        $kategori = $_GET['kat'];
        $data = $this->data;
        $opt['conditions'] = array('id_kat_peserta');
        $kat_peserta = $this->Peserta_model->get_kat_peserta($kategori);
        $data['level_2'] = $kat_peserta->kategori . ' Calon Penerima Anugerah';
        $data['kategori'] = $kategori;
        $this->render('manajemen_peserta_view', $data);
//        print_r($kat_peserta);
    }

    public function list_peserta($kategori) {
        $data = array();
        $data = $this->Peserta_model->get_peserta(array('kategori' => $kategori));
        if (count($data) > 0) {
            foreach ($data as $k => $x) {
                $peserta[] = array(
                    $x->id_peserta,
                    $x->nama_peserta,
                    $x->email_peserta,
                    $x->jabatan,
                    $x->provinsi,
                    $x->kota,
                    $x->no_hp_peserta,
                    "<a href='user/detail?id=$x->id_user' class='btn btn-edit sbold blue'> Detail Pengusul </a>",
                    "<a href='detail?id=$x->id_peserta' class='btn btn-edit sbold blue'> Detail Calon Penerima </a>"
//                    "<button href='#ModalEdit' role='button' data-toggle='modal' class='btn btn-edit sbold yellow'> Edit <i class='fa fa-edit'></i></button>",
//                    "<button href='#ModalAktivasi' role='button' data-toggle='modal' class='btn btn-edit sbold green-meadow'> $text </button>"
                );
            }
        } else {
            $peserta = array();
        }
        $peserta = array('data' => $peserta);
        echo json_encode($peserta);
    }

    public function detail() {
        //detail peserta
        $data = $this->data;
        if ($this->session->userdata('id_profil') != 5) {
            $id_peserta = $_GET['id'];
        } else {
            $opt['conditions'] = array('id_user' => $this->session->userdata('id_user'));
            $peserta = $this->Peserta_model->paginate_read($opt);
            if (count($peserta) < 1) {
                redirect('peserta/add_peserta');
            } else {
                $id_peserta = $peserta->id_peserta;
            }
        }
        $where = array('id_peserta' => $id_peserta);
        $peserta = $this->Peserta_model->get_peserta($where);
        $data['peserta'] = $peserta[0];
        $data['level_2'] = $peserta[0]->nama_peserta;
        $opt['conditions'] = array('aktif' => 'Y');
        $kat_komp = $this->Kat_komp_model->paginate($opt);
        foreach ($kat_komp as $x):
            $jawaban = $this->Jawaban_model->get_jawaban($x->id_kat_komp,$id_peserta);
//            $jawaban['path_data'] = 'uploads/data'.$id_peserta.$jawaban->id_komponen;
            $data[$x->nama_kat_komp] = $jawaban;
        endforeach;
//        echo '<pre>';
//        print_r($jawaban);

        $this->render('detail_peserta_view', $data);
    }

    public function add_peserta() {
        $data = $this->data;
        $opt['conditions'] = array('id_user' => $this->session->userdata('id_user'));
        if (count($this->Peserta_model->paginate_read($opt)) > 0) {
            redirect('peserta/detail');
        }
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $data['kode_prov'] = $this->Kode_wilayah_model->paginate(array('kode_wilayah' => '000000'));
        $data['kat_peserta'] = $this->Peserta_model->get_kat();
        $this->render('add_peserta_view', $data);
    }

    public function post_add_peserta() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            redirect('dashboard');
        }

        /* VALIDATION */
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');
        $this->form_validation->set_rules('id_provinsi', 'Provinsi', 'required');
        $this->form_validation->set_rules('nama_peserta', 'Nama Peserta', 'required');
        $this->form_validation->set_rules('email_peserta', 'Email Peserta', 'required|valid_email|is_unique[peserta.email_peserta]');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('no_hp_peserta', 'No. HP Peserta', 'required');
        $this->form_validation->set_rules('alamat_kantor', 'Alamat Kantor', 'required');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        $Peserta = new Peserta_model();
        $Peserta->kategori = $this->input->post('kategori');
        $Peserta->jenis_kelamin = $this->input->post('jenis_kelamin');
        $Peserta->email_peserta = $this->input->post('email_peserta');
        $Peserta->id_provinsi = $this->input->post('id_provinsi');
        if ($this->input->post('id_kota') == null) {
            $Peserta->id_kota = 0;
        } else {
            $Peserta->id_kota = $this->input->post('id_kota');
        }
        $Peserta->nama_peserta = $this->input->post('nama_peserta');
        $Peserta->jabatan = $this->input->post('jabatan');
        $Peserta->no_hp_peserta = $this->input->post('no_hp_peserta');
        $Peserta->alamat_kantor = $this->input->post('alamat_kantor');
        $Peserta->id_user = $this->session->userdata('id_user');
        $Peserta->created_at = Carbon::now()->toDateTimeString();

        if ($this->form_validation->run() == FALSE) {
            $this->add_peserta();
        } else {
            if (!$Peserta->save()) {
                $this->add_peserta();
            } else {
                /* Log Activity */
//                $jsonString = json_encode($_POST);
//                logging("Adding Pages", $this->session->userdata("id"), $jsonString);
//                $this->session->set_flashdata("success", "Data berhasil tersimpan");
                redirect('peserta/detail');
            }
        }
    }

    public function get_kota() {
        $mst_kode_wilayah = $this->input->post('id_provinsi');
        $opt['conditions'] = array('mst_kode_wilayah' => $mst_kode_wilayah);
        $kota = $this->Kode_wilayah_model->paginate($opt);
        echo "<option value=''>Pilih Kota/ Kab</option>";
        foreach ($kota as $dkota):
            echo "<option value='" . $dkota->kode_wilayah . "'>" . $dkota->nama . "</option>";
        endforeach;
    }

    public function get_nilai($id_kat_komp,$id_peserta) {
        $opt_pes['conditions'] = array('id_peserta'=>$id_peserta);
        $kat_peserta = $this->Peserta_model->paginate_read($opt_pes);
        $opt_komp['conditions'] = array('id_kat_peserta'=>$kat_peserta->kategori,'id_kat_komp'=>$id_kat_komp);
        $jml_komp = $this->Komponen_model->paginate($opt_komp);
        $jml_jawaban = $this->Jawaban_model->get_jawaban($id_kat_komp,$id_peserta);
        print_r(count($jml_jawaban));
    }

}

?>