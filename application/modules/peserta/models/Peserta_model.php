<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peserta_model extends MY_Model {

    static $table = "peserta";

    public function __construct() {
        parent::__construct();
    }
    
    public function get_kat_peserta($id_kat_peserta){
        $this->db->where('id_kat_peserta',$id_kat_peserta);
        $query = $this->db->get('ref_kat_peserta');
        return $query->row(0, get_class($this));
    }
    
    public function get_kat($where=array()){
        if($where != ""){
            $this->db->where($where);
        }
        $query = $this->db->from('ref_kat_peserta')->get();
        return $query->result();
    }

    public function get_peserta($where=array()){
        $this->db->select('a.id_user,a.id_peserta, a.nama_peserta, a.email_peserta, a.jabatan, b.nama as provinsi, c.nama as kota, a.no_hp_peserta')
                ->from(static::$table.' a')
                ->join('ref_kode_wilayah b','a.id_provinsi=b.kode_wilayah','left')
                ->join('ref_kode_wilayah c','a.id_kota=c.kode_wilayah','left')
                ->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_peserta_detail($id_peserta){
//        $this->db->select('')
    }

}
