<link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/scripts/profile.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/scripts/charts-amcharts.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<style>
    .item {
        padding:10px !important;
        border: 1px dashed #4e5f73 !important;
        margin:2px !important;
    }
    
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar" style="width:500px !important;">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?= $peserta->nama_peserta; ?>
                    </div>
                    <div class="profile-usertitle-job">
                        <?= $peserta->jabatan; ?> - <?= ($peserta->kota == '-') ? $peserta->provinsi : $peserta->kota; ?>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="#">
                                <i class="icon-home"></i>
                                Overview </a>
                        </li>
                        <li class="">
                                <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                <input type="hidden" name="type" value="all">
                                <i class='fa fa-download'></i><input type="submit" name="submit" value="Download Semua Data" class="btn btn-primary">
                            </form>
                                
                                
                        </li>
                        <!--                        <li>
                                                    <a href="extra_profile_account.html">
                                                        <i class="icon-settings"></i>
                                                        Account Settings </a>
                                                </li>-->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="uppercase profile-stat-title">
                            37
                        </div>
                        <div class="uppercase profile-stat-text">
                            Kebijakan
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="uppercase profile-stat-title">
                            51
                        </div>
                        <div class="uppercase profile-stat-text">
                            Anggaran
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="uppercase profile-stat-title">
                            61
                        </div>
                        <div class="uppercase profile-stat-text">
                            Program
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="uppercase profile-stat-title">
                            61
                        </div>
                        <div class="uppercase profile-stat-text">
                            Implementasi
                        </div>
                    </div>
                </div>
                <!-- END STAT -->
            </div>
            <div class="portlet light">
                <!-- STAT -->
                <div id="chart_9" class="chart" style="height: 400px;">
                </div>
                <!-- END STAT -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Kebijakan</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <div class="general-item-list">
                                    <?php foreach ($Kebijakan as $x): ?>
                                        <div class="item">
                                            <div class="item-body">
                                                <p><?= $x->komponen; ?></p>
                                                <p><span class="item-name primary-link">Jawaban : </span><?= $x->jawaban_1; ?><?= ($x->jawaban_2 != null && $x->label != null) ? ' ' . $x->label . ' ' . $x->jawaban_2 : ''; ?></p>
                                                <p><span class="item-label"><?= $x->created_at; ?></span></p>
                                            </div>
                                            <div class="item-head">
                                                <div class="item-details">
                                                    <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                                        <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                                        <input type="hidden" name="nama_kat_komp" value="<?= $x->nama_kat_komp; ?>">
                                                        <input type="hidden" name="id_komponen" value="<?= $x->id_komponen; ?>">
                                                        <input type="hidden" name="type" value="komponen">
                                                        <input type="submit" name="submit" value="Download Data" class="btn green btn-circle">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                <input type="hidden" name="nama_kat_komp" value="Kebijakan">
                                <input type="hidden" name="type" value="kat_komp">
                                <input type="submit" name="submit" value="Download Semua Data Kebijakan" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Anggaran</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <div class="general-item-list">
                                    <?php foreach ($Anggaran as $x): ?>
                                        <div class="item">
                                            <div class="item-body">
                                                <p><?= $x->komponen; ?></p>
                                                <p><span class="item-name primary-link">Jawaban : </span><?= $x->jawaban_1; ?><?= ($x->jawaban_2 != null && $x->label != null) ? ' ' . $x->label . ' ' . $x->jawaban_2 : ''; ?></p>
                                            </div>
                                            <div class="item-head">
                                                <div class="item-details">
                                                    <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                                        <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                                        <input type="hidden" name="nama_kat_komp" value="<?= $x->nama_kat_komp; ?>">
                                                        <input type="hidden" name="id_komponen" value="<?= $x->id_komponen; ?>">
                                                        <input type="hidden" name="type" value="komponen">
                                                        <input type="submit" name="submit" value="Download Data" class="btn green btn-circle">
                                                    </form>
                                                    <span class="item-label"><?= $x->created_at; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                <input type="hidden" name="nama_kat_komp" value="Anggaran">
                                <input type="hidden" name="type" value="kat_komp">
                                <input type="submit" name="submit" value="Download Semua Data Anggaran" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Program</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <div class="general-item-list">
                                    <?php foreach ($Program as $x): ?>
                                        <div class="item">
                                            <div class="item-body">
                                                <p><?= $x->komponen; ?></p>
                                                <p><span class="item-name primary-link">Jawaban : </span><?= $x->jawaban_1; ?><?= ($x->jawaban_2 != null && $x->label != null) ? ' ' . $x->label . ' ' . $x->jawaban_2 : ''; ?></p>
                                            </div>
                                            <div class="item-head">
                                                <div class="item-details">
                                                    <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                                        <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                                        <input type="hidden" name="nama_kat_komp" value="<?= $x->nama_kat_komp; ?>">
                                                        <input type="hidden" name="id_komponen" value="<?= $x->id_komponen; ?>">
                                                        <input type="hidden" name="type" value="komponen">
                                                        <input type="submit" name="submit" value="Download Data" class="btn green btn-circle">
                                                    </form>
                                                    <span class="item-label"><?= $x->created_at; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                <input type="hidden" name="nama_kat_komp" value="Program">
                                <input type="hidden" name="type" value="kat_komp">
                                <input type="submit" name="submit" value="Download Semua Data Program" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Implementasi</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <div class="general-item-list">
                                    <?php foreach ($Implementasi as $x): ?>
                                        <div class="item">
                                            <div class="item-body">
                                                <p><?= $x->komponen; ?></p>
                                                <p><span class="item-name primary-link">Jawaban : </span><?= $x->jawaban_1; ?><?= ($x->jawaban_2 != null && $x->label != null) ? ' ' . $x->label . ' ' . $x->jawaban_2 : ''; ?></p>
                                            </div>
                                            <div class="item-head">
                                                <div class="item-details">
                                                    <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                                        <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                                        <input type="hidden" name="nama_kat_komp" value="<?= $x->nama_kat_komp; ?>">
                                                        <input type="hidden" name="id_komponen" value="<?= $x->id_komponen; ?>">
                                                        <input type="hidden" name="type" value="komponen">
                                                        <input type="submit" name="submit" value="Download Data" class="btn green btn-circle">
                                                    </form>
                                                    <span class="item-label"><?= $x->created_at; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <form method="post" action="<?= $this->config->base_url(); ?>data/zip_download">
                                <input type="hidden" name="id_peserta" value="<?= $peserta->id_peserta; ?>">
                                <input type="hidden" name="nama_kat_komp" value="Implementasi">
                                <input type="hidden" name="type" value="kat_komp">
                                <input type="submit" name="submit" value="Download Semua Data Implementasi" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        Profile.init(); // init page demo
        var chart = AmCharts.makeChart("chart_9", {
            "type": "radar",
            "categoryField": "category",
            "graphs": [
                {
                    "valueField": "value"
                }
            ],
            "valueAxes": [
                {
                    "axisTitleOffset": 20,
                    "minimum": 0,
                    "axisAlpha": 0.15,
                    "dashLength": 3
                }
            ],
            "dataProvider": [
                {
                    "category": "Keb",
                    "value": 10
                },
                {
                    "category": "Ang",
                    "value": 10
                },
                {
                    "category": "Prog",
                    "value": 10
                },
                {
                    "category": "Impl",
                    "value": 5
                }
            ],
            "autoResize": true,
            "autoDisplay": true
        });
    });
</script>