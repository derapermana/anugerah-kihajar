<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Carbon\Carbon;

if (!function_exists('theresAnyErrorMessages')) {

    function theresAnyErrorMessages() {
        $CI = & get_instance();
        return $CI->session->flashdata("error") != '';
    }

}

if (!function_exists('theresAnySuccessMessagesA')) {

    function theresAnySuccessMessages() {
        $CI = & get_instance();
        return $CI->session->flashdata("success") != '';
    }

}

if (!function_exists('getErrorNotification')) {

    function getErrorNotification() {
        $CI = & get_instance();
        $messages = $CI->session->flashdata("error");

        $display = "<div class='alert alert-danger'>";
        $display .= $messages;
        $display .= "</div>";

        return $display;
    }

}

if (!function_exists('getSuccessNotification')) {

    function getSuccessNotification() {

        $CI = & get_instance();

        $messages = $CI->session->flashdata("success");

        $display = "<div class='alert alert-success'>";
        $display .= $messages;
        $display .= "</div>";

        return $display;
    }

}

if (!function_exists('getNotification')) {

    function getNotification() {
        if (theresAnyErrorMessages()) {
            echo getErrorNotification();
        }
        if (theresAnySuccessMessages()) {
            echo getSuccessNotification();
        }
    }

}

if (!function_exists('logging')) {

    function logging($message, $author_id, $data = "") {


        $CI = & get_instance();
        $loggingIsActive = $CI->config->item("logging");

        if ($loggingIsActive) {
            $CI->load->model("log/Log_model");

            $Log = new Log_model();
            $Log->name = $message;
            $Log->author = $author_id;
            $Log->data = $data;
            $Log->created_at = Carbon::now()->toDateTimeString();
            $Log->save();
        }
    }

}

if (!function_exists('dd')) {

    function dd($variable) {
        var_dump($variable);
        exit;
    }

}

if (!function_exists('isLogin')) {

    function isLogin() {
        $CI = & get_instance();
        return $CI->session->userdata("id") != '' ? TRUE : FALSE;
    }

}

