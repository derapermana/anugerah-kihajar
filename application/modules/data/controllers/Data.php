<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Data extends MY_Controller {

    public $data = array('current_page' => "data");

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Data_model');
        $this->load->model('peserta/Peserta_model');
        $this->load->model('komponen/Jawaban_model');
        $this->load->model('komponen/Komponen_model');
    }

    public function kebijakan() {
        $data = $this->data;

        $this->render('data_view', $data);
    }

    public function peserta() {
        $opt['conditions'] = array('id_user' => $this->session->userdata('id_user'));
        $peserta = $this->Peserta_model->paginate_read($opt);
        return $peserta;
    }

    public function upload() {
        $data = $this->input->post();
//        print_r($data);
        $id_jawaban = $this->input->post('id_jawaban');
        $opt_jwb['conditions'] = array('id_jawaban' => $id_jawaban);
        $jawaban = $this->Jawaban_model->paginate_read($opt_jwb);
        $kat_komp = $this->Komponen_model->get_kat_komp($jawaban->id_komponen);
        if ($this->Data_model->upload_data($jawaban->id_peserta, $jawaban->id_komponen, $kat_komp->nama_kat_komp) == '') {
            $upload_data = $this->upload->data();
            $jenis_data = $upload_data['file_ext'];
            $path_data = $upload_data['full_path'];
            $file_name = $upload_data['file_name'];
            $Data = new Data_model();
            $Data->id_jawaban = $id_jawaban;
            $Data->jenis_data = $jenis_data;
            $Data->path_data = $path_data;
            $Data->file_name = $file_name;
            $Data->created_at = Carbon::now()->toDateTimeString();
            if ($Data->save()) {
                $this->session->set_flashdata('success_msg', 'Data berhasil disimpan');
            } else {
                $this->session->set_flashdata('error_msg', 'Data gagal disimpan!' . $this->Data_model->upload->display_errors());
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Data gagal di simpan!' . $this->Data_model->upload->display_errors());
        }
        redirect('komponen?id=' . $this->session->flashdata('kat_komp'));
    }

    public function zip_download() {
        $this->load->library('zip');
        $id_peserta = $this->input->post('id_peserta');
        $path_data = 'uploads/data/' . $id_peserta . '/';
        $type = $this->input->post('type');
        if ($type == 'all') {
            $path = $path_data;
        } else if ($type == 'kat_komp') {
            $nama_kat_komp = $this->input->post('nama_kat_komp');
            $path = $path_data . $nama_kat_komp . '/';
        } else if ($type == 'komponen') {
            $nama_kat_komp = $this->input->post('nama_kat_komp');
            $id_komponen = $this->input->post('id_komponen');
            $path = $path_data . $nama_kat_komp . '/' . $id_komponen . '/';
        }
//        $path = 'uploads/data/12/Kebijakan/1/';
        $this->zip->read_dir($path);
//        print_r($path);
// Download the file to your desktop. Name it "my_backup.zip"
        $this->zip->download('coba.zip');
    }

    public function delete_file($id_data = "") {
        $this->load->helper('file');
        $data = $this->Data_model->get_path_data($id_data);
        if (count($data) < 1) {
            show_404();
        }
        $path = 'uploads/data/' . $data->id_peserta . '/' . $data->nama_kat_komp . '/' . $data->id_komponen . '/' .$data->file_name;
//        $path = $this->config->base_url().'uploads/data/12/Anggaran/5/1_(1).png';
//        unlink($path);
//        print_r($path);
        if(unlink($path)) {
//            echo 'ok';
            $Data = new Data_model();
            $Data->id_data = $id_data;
            if ($Data->delete('id_data')) {
                $this->session->set_flashdata('success_msg', 'Data berhasil dihapus');
            } else {
                $this->session->set_flashdata('error_msg', 'Data gagal dihapus!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Data gagal dihapus!');
        }
        redirect('komponen?id=' . $this->session->flashdata('kat_komp'));
    }

}

?>