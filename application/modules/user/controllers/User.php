<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class User extends MY_Controller {

    public $data = array('current_page' => 'user');

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('User_model');
    }

    public function login() {
        if ($this->session->userdata('is_login')) {
            redirect(base_url('dashboard'));
        }
        if ($this->input->post('submit')) {

            $this->form_validation->set_rules('email', 'Email', 'required|trim|callback_verify_login');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

            if ($this->form_validation->run()) {
                $user = $this->User_model->login();
                $this->session->set_userdata('is_login', TRUE);
                $this->session->set_userdata('id_user', $user->id_user);
                $this->session->set_userdata('nama_user', $user->nama_user);
                $this->session->set_userdata('id_profil', $user->id_profil);
                $this->session->set_userdata('aktif', $user->aktif);
                $_SESSION['id_user'] = $user->id_user;
                session_start();
                redirect('dashboard');
            }
        }
        $this->load->view('login_view');
    }

    public function verify_login() {
        if ($this->User_model->login()) {
            return TRUE;
        } else {
            $this->form_validation->set_message('verify_login', 'Username atau Password Anda tidak sah!!');
            return FALSE;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        session_destroy();
        redirect('user/login');
    }

    public function juri() {
        $data = $this->data;
        $data['level_2'] = 'Manajemen Juri';
        $data['id_profil'] = 2;
        $this->render('manajemen_user_view', $data);
    }

    public function registrasi() {
        if ($this->session->userdata('is_login')) {
            redirect(base_url('dashboard'));
        }
        if ($this->input->post('submit')) {


            $this->form_validation->set_rules('nama_user', 'Nama Lengkap', 'required');
            $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
            $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
            $this->form_validation->set_rules('email_user', 'Email', 'required|trim|valid_email|is_unique[user.email_user]');
            $this->form_validation->set_rules('no_hp_user', 'No Handphone', 'required|trim|is_natural');
            $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
            $this->form_validation->set_rules('instansi', 'Instansi', 'required');
            $this->form_validation->set_rules('alamat_kantor', 'Alamat Instansi', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            $this->form_validation->set_rules('konf_password', 'Konfirmasi Password', 'required|min_length[6]|matches[password]');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

            if ($this->form_validation->run()) {
                $config['upload_path'] = './uploads/surat_tugas';
                $config['allowed_types'] = 'pdf|jpg|jpeg|png';
                $config['max_size'] = 1000;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('surat')) {
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('registrasi_view', $error);
                } else {
                    $User = new User_model();
                    
                    $User->nama_user = $this->input->post('nama_user',true);
                    $User->jenis_kelamin = $this->input->post('jenis_kelamin',true);
                    $User->tempat_lahir = $this->input->post('tempat_lahir',true);
                    $User->tanggal_lahir = $this->input->post('tanggal_lahir',true);
                    $User->email_user = $this->input->post('email_user',true);
                    $User->no_hp_user = $this->input->post('no_hp_user',true);
                    $User->jabatan = $this->input->post('jabatan',true);
                    $User->instansi = $this->input->post('instansi',true);
                    $User->alamat_kantor = $this->input->post('alamat_kantor',true);
                    $User->password = sha1($this->config->item('encryption_key') . $this->input->post('password',true));
                    $User->id_profil = 5;
                    $User->created_at = Carbon::now()->toDateTimeString();
                    $User->surat = $this->upload->data('file_name'); 
                    $User->aktif = 0;
                    if($User->save()){
                        echo 'ok';
                    } else {
                        $error = array('error','Gagal Registrasi');
                        $this->load->view('registrasi_view', $error);
                    }
                }
            }
        }
        $this->load->view('registrasi_view');
    }

    public function pengusul() {
        $data = $this->data;
        $data['level_2'] = 'Manajemen Pengusul';
        $data['id_profil'] = 5;
        $this->render('manajemen_user_view', $data);
    }

    public function list_user($id_profil) {
        $data = array();
        $opt['conditions'] = array('id_profil' => $id_profil);
        $data = $this->User_model->paginate($opt);
        if (count($data) > 0) {
            foreach ($data as $k => $x) {
                $blokir = ($x->aktif == 0) ? 'Non Aktif' : 'Aktif';
                $text = ($x->aktif == 0) ? 'Atifkan' : 'Non Aktifkan';
                $operator[] = array(
                    $x->id_user,
                    $x->nama_user,
                    $x->jenis_kelamin,
                    $x->email_user,
                    $x->jabatan,
                    $x->instansi,
                    $x->no_hp_user,
                    $blokir,
                    "<a href='detail?id=$x->id_user' class='btn btn-edit sbold yellow'> Detail </a>"
//                    "<button href='#ModalEdit' role='button' data-toggle='modal' class='btn btn-edit sbold yellow'> Edit <i class='fa fa-edit'></i></button>",
//                    "<button href='#ModalAktivasi' role='button' data-toggle='modal' class='btn btn-edit sbold green-meadow'> $text </button>"
                );
            }
        } else {
            $operator = array();
        }
        $operator = array('data' => $operator);
        echo json_encode($operator);
    }

}

?>