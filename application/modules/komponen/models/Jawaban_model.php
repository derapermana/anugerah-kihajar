<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jawaban_model extends MY_Model {

    static $table = "jawaban";

    public function __construct() {
        parent::__construct();
    }
    
    public function get_jawaban($id_kat_komp,$id_peserta){
        $this->db
                ->from(static::$table.' a')
                ->join('komponen b','a.id_komponen=b.id_komponen')
                ->join('ref_kat_komp c','b.id_kat_komp=c.id_kat_komp')
                ->where('c.id_kat_komp',$id_kat_komp)
                ->where('a.id_peserta',$id_peserta);
        $query = $this->db->get();
        return $query->result();
    }

}
