<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kat_komp_model extends MY_Model {

    static $table = "ref_kat_komp";

    public function __construct() {
        parent::__construct();
    }

}
