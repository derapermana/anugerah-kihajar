<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public function save($real_id = "") {
        if ($real_id != ""):
            return isset($this->$real_id) ? $this->update($real_id) : $this->create($real_id); // A new record won't have an id yet
        else:
            return isset($this->id) ? $this->update() : $this->create(); // A new record won't have an id yet.
        endif;
    }

    public function create() {

        if ($this->db->insert(static::$table, $this)) {
            $this->id = $this->db->insert_id();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update($real_id = "") {
        if ($real_id != ""):
            $this->db->where($real_id, $this->$real_id);
        else:
            $this->db->where('id', $this->id);
        endif;
        $this->db->update(static::$table, $this);
        return ( $this->db->affected_rows() == 1 ) ? TRUE : FALSE;
    }

    public function delete($real_id = "") {
        if ($real_id != ""):
            $this->db->where($real_id, $this->$real_id);
        else:
            $this->db->where('id', $this->id);
        endif;
        $this->db->delete(static::$table);
        return ( $this->db->affected_rows() == 1 ) ? TRUE : FALSE;
    }

    public function read($id, $real_id = "") {
        if ($real_id != ""):
            $this->db->where($real_id, $id);
        else:
            $this->db->where('id', $id);
        endif;

        $query = $this->db->get(static::$table);
        return $query->row(0, get_class($this)); // return as object - for editing purpose
        // $result_array = $query->result();
        // return !empty( $result_array ) ? array_shift( $result_array ) : FALSE;
    }

    public function get_all($order = "", $array = "") {
        if (!empty($array))
            $this->db->select($array);
        if (!empty($order))
            $this->db->order_by($order);
        $query = $this->db->get(static::$table);
        return $query->result();
    }

    public function paginate($opt) {
        $opt["page"] = isset($opt["page"]) ? $opt["page"] : "";
        if (isset($opt["join"]) && isset($opt["select"]) && isset($opt["join_condition"])) {
            if (isset($opt["join2"]) && isset($opt["join_condition2"])) {

                if (isset($opt["join3"]) && isset($opt["join_condition3"])) {

                    $this->db->select($opt["select"]);
                    $this->db->join($opt["join"], $opt["join_condition"], 'left');
                    $this->db->join($opt["join2"], $opt["join_condition2"], 'left');
                    $this->db->join($opt["join3"], $opt["join_condition3"], 'left');
                } else {
                    $this->db->select($opt["select"]);
                    $this->db->join($opt["join"], $opt["join_condition"], 'left');
                    $this->db->join($opt["join2"], $opt["join_condition2"], 'left');
                }
            } else {

                $this->db->select($opt["select"]);
                $this->db->join($opt["join"], $opt["join_condition"], 'left');
            }
        }

        if (isset($opt['select'])) {
            $this->db->select($opt['select']);
        }

        if (isset($opt['conditions'])) {
            $this->db->where($opt['conditions']);
        }
        if (isset($opt['group'])) {
            $this->db->group_by($opt['group']);
        }
        if (isset($opt['sort'])) {
            $order = ( $opt['order'] != "" && in_array(strtolower($opt['order']), array('asc', 'desc')) ) ? strtolower($opt['order']) : 'asc';
            $this->db->order_by($opt['sort'], $order);
        }
        if (isset($opt['sort_multiple'])) {
            $this->db->order_by($opt['sort_multiple']);
        }
        if (isset($opt['limit']) && $opt['limit'] > 0) {
            $page = $opt['page'] != "" ? $opt['page'] : 0;
            if (!empty($opt['page'])):
                //$page=(($page-1)*$limit)+1;
                $start = ( $page - 1 ) * $opt['limit'];
            else:
                //$page=0;
                $start = $page;
            endif;

            //$start=$page;
            $this->db->limit($opt['limit'], $start);
        }

        $query = $this->db->get(static::$table . " a");
        return $query->result();
    }

    public function paginate_read($opt) {
//            $opt["join"] = isset($opt["join"]) ? $opt["join"] : "";
//
//            $opt["select"]= isset($opt["select"])? $opt["select"] : "";
//            $opt["join_condition"]=  isset($opt["join_condition"])?$opt["join_condition"]:"";

        if (isset($opt["join"]) && isset($opt["select"]) && isset($opt["join_condition"])) {
            if (isset($opt["join2"]) && isset($opt["join_condition2"])) {

                if (isset($opt["join3"]) && isset($opt["join_condition3"])) {

                    $this->db->select($opt["select"]);
                    $this->db->join($opt["join"], $opt["join_condition"], 'left');
                    $this->db->join($opt["join2"], $opt["join_condition2"], 'left');
                    $this->db->join($opt["join3"], $opt["join_condition3"], 'left');
                } else {
                    $this->db->select($opt["select"]);
                    $this->db->join($opt["join"], $opt["join_condition"], 'left');
                    $this->db->join($opt["join2"], $opt["join_condition2"], 'left');
                }
            } else {

                $this->db->select($opt["select"]);
                $this->db->join($opt["join"], $opt["join_condition"], 'left');
            }
        }

        if (isset($opt['select'])) {
            $this->db->select($opt['select']);
        }

        if (isset($opt['conditions'])) {
            $this->db->where($opt['conditions']);
        }
        if (isset($opt['group'])) {
            $this->db->group_by($opt['group']);
        }
        if (isset($opt['sort'])) {
            $order = ( $opt['order'] != "" && in_array(strtolower($opt['order']), array('asc', 'desc')) ) ? strtolower($opt['order']) : 'asc';
            $this->db->order_by($opt['sort'], $order);
        }
        if (isset($opt['sort_multiple'])) {
            $this->db->order_by($opt['sort_multiple']);
        }
        if (isset($opt['limit']) && $opt['limit'] > 0) {
            $page = $opt['page'] != "" ? $opt['page'] : 1;
            $start = ( $page - 1 ) * $opt['limit'];
            $this->db->limit($opt['limit'], $start);
        }

        $query = $this->db->get(static::$table . " a");
        return $query->row();
    }

    public function count($conditions = "") {
        if ($conditions != "") {
            $this->db->where($conditions);
        }
        return $this->db->count_all_results(static::$table);
    }

    public function batch_insert($data = array()) {
        $this->db->insert_batch(static::$table, $data);
    }

    public function get_where($where = array()) {
        return $this->db->where($where)->from(static::$table)->get()->result();
    }

}

/* End of file */
