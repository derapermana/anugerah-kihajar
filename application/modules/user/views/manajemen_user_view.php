<link rel="stylesheet" type="text/css" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<script type="text/javascript" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo (isset($level_2) ? $level_2 : $current_page); ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="tbl-tambah" class="btn green">
                                    Add New <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer" id="table_user" role="grid" aria-describedby="sample_1_info">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Email</th>
                                <th>Jabatan</th>
                                <th>Instansi</th>
                                <th>No Telp</th>
                                <th></th>
                                <th></th>
<!--                                    <th></th>
                                <th></th>-->
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = $('#table_user').DataTable({
            "ajax": "list_user/<?= $id_profil; ?>"
//            responsive: "true",
//            "columnDefs": [{
//                    "targets": -1,
//                    "orderable": false,
//                    "defaultContent": ""
//                }],
        });
    });
</script>