<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Komponen_model extends MY_Model {

    static $table = "komponen";

    public function __construct() {
        parent::__construct();
    }
    
    public function get_kat_komp($id_komponen){
        $this->db->from(static::$table.' a')
                ->join('ref_kat_komp b','a.id_kat_komp=b.id_kat_komp')
                ->where('a.id_komponen',$id_komponen);
        $query = $this->db->get();
        return $query->row(0, get_class($this));
    }

}
