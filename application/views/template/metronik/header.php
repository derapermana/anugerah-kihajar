<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.3.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Anugerah Kihajar 2016 | <?=$current_page;?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
        <script src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/scripts/demo.js" type="text/javascript"></script>
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                Demo.init(); // init demo features
            });
        </script>
        <!-- END JAVASCRIPTS -->

        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed page-quick-sidebar-over-content">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="#">
                        <img src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/img/logo.png" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle hide1" src="<?= $this->config->base_url() . ACTIVE_TEMPLATE; ?>admin/layout/img/avatar3_small.jpg"/>
                                <span class="username username-hide-on-mobile">
                                    <?= $this->session->userdata('nama_user'); ?> </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <!--                                <li>
                                                                    <a href="extra_profile.html">
                                                                        <i class="icon-user"></i> My Profile </a>
                                                                </li>
                                                                <li>
                                                                    <a href="page_calendar.html">
                                                                        <i class="icon-calendar"></i> My Calendar </a>
                                                                </li>
                                                                <li>
                                                                    <a href="inbox.html">
                                                                        <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
                                                                            3 </span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="page_todo.html">
                                                                        <i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
                                                                            7 </span>
                                                                    </a>
                                                                </li>
                                                                <li class="divider">
                                                                </li>
                                                                <li>
                                                                    <a href="extra_lock.html">
                                                                        <i class="icon-lock"></i> Lock Screen </a>
                                                                </li>-->
                                <li>
                                    <a href="<?= $this->config->base_url(); ?>user/logout">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>