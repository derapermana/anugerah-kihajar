<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('berkasgetfiles')) {

    function berkasgetfiles($idPeserta = 0, $idKatKomp = 0) {
        $ci = & get_instance();
        $ci->load->library('zip');
        //
        $filename = date('YmdHis') . ".zip";
        // $filepath=$_SERVER['DOCUMENT_ROOT']."/upload/data/".intval($idPeserta)."/".intval($idKatKomp);
        $zippath = $_SERVER['DOCUMENT_ROOT'] . "/upload/data/" . intval($idPeserta) . "/" . intval($idKatKomp) . "/" . $filename;
        //
        $ci->db->select("id_data,file_name,path_data");
        $xdata = $ci->db->get_where("data", array("id_peserta" => intval($idPeserta), "id_kat_komp" => intval($idKatKomp)));
        $xresults = $xdata->result();
        // echo $ci->db->last_query();
        // print_r($xresults);
        //
		if (count($xresults) > 0) {
            foreach ($xresults as $k => $x) {
                $ci->zip->read_file($x->path_data);
                // echo $x->path_data;
            }
            $ci->zip->archive($zippath);
            $ci->zip->download($filename);
        } else {
            $name = 'No File Found';
            $data = '';
            $ci->zip->add_data($name, $data);
            $ci->zip->download($filename);
        }
    }

}
?>