<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data_model extends MY_Model {

    static $table = "data";

    public function __construct() {
        parent::__construct();
    }

    public function get_detail_data($where = array()) {
        $this->db->select('a.*,b.komponen,b.label')
                ->from(static::$table . ' a')
                ->join('komponen b', 'a.id_komp=b.id_komponen', 'left')
                ->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function upload_data($id_peserta, $id_komponen, $nama_kat_komp) {

        $config['upload_path'] = './uploads/data/' . $id_peserta . '/' .$nama_kat_komp. '/' .$id_komponen;
        $config['allowed_types'] = 'mp4|doc|docx|xls|xlsx|pdf|jpg|jpeg|zip|png|rar';
        $config['max_size'] = '25000';
//        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        $dir_exist = true; // flag for checking the directory exist or not
        if (!is_dir('./uploads/data/' . $id_peserta . '/' .$nama_kat_komp. '/' .$id_komponen)) {
            mkdir('./uploads/data/' . $id_peserta . '/' .$nama_kat_komp. '/' .$id_komponen, 0777, true);
            $dir_exist = false; // dir not exist
        } else {
            
        }
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {
            // upload failed
            //delete dir if not exist before upload
            if (!$dir_exist) {
                rmdir('./uploads/data/' . $id_peserta . '/' .$nama_kat_komp. '/' .$id_komponen);
            }
            return $this->upload->display_errors();
        } else {
            // upload success

            return '';
        }
//        return $tes;
    }
    
    public function get_path_data($id_data){
        $this->db->select('b.id_peserta AS id_peserta, d.nama_kat_komp AS nama_kat_komp, c.id_komponen AS id_komponen, a.file_name AS file_name')
                ->from(static::$table.' a')
                ->join('jawaban b','a.id_jawaban=b.id_jawaban')
                ->join('komponen c','b.id_komponen=c.id_komponen')
                ->join('ref_kat_komp d','c.id_kat_komp=d.id_kat_komp')
                ->where('a.id_data',$id_data);
        $query = $this->db->get();
        return $query->row(0, get_class($this));
    }

}
