<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Komponen extends MY_Controller {

    public $data = array('current_page' => "komponen");

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Kat_komp_model');
        $this->load->model('Var_komp_model');
        $this->load->model('Komponen_model');
        $this->load->model('data/Data_model');
        $this->load->model('peserta/Peserta_model');
        $this->load->model('Jawaban_model');
    }

    public function index() {
        $data = $this->data;
        if (!isset($_GET['id'])) {
            show_404();
        } else if ($_GET['id'] == "") {
            show_404();
        } else if (count($this->Kat_komp_model->paginate_read(array('conditions' => array('id_kat_komp' => $_GET['id'])))) < 1) {
            show_404();
        }
        $id_kat_komp = $_GET['id'];
        $opt_kat['conditions'] = array('id_kat_komp' => $id_kat_komp);
        $kat_komp = $this->Kat_komp_model->paginate_read($opt_kat);
        $data['kat_komp'] = $kat_komp;
        $komponen = $this->Komponen_model->paginate(array('conditions' => array('id_kat_komp' => $id_kat_komp)));
        $data['komponen'] = $komponen;
        $data['level_2'] = $kat_komp->nama_kat_komp;
        $this->render('komponen_view', $data);
    }

    public function list_komponen($id_kat_komp) {
        $peserta = $this->Peserta_model->paginate_read(array('conditions' => array('id_user' => $this->session->userdata('id_user'))));
        $data_komp = array();
        $opt['conditions'] = array('id_kat_komp' => $id_kat_komp, 'aktif' => 'Y', 'id_kat_peserta' => $peserta->kategori);
        $data_komp = $this->Komponen_model->paginate($opt);
        if (count($data_komp) > 0) {
            foreach ($data_komp as $k => $x) {
                $jawaban = $this->cek_jawaban($x->id_komponen, $peserta->id_peserta);
                if ($jawaban['jawaban'] == 'Belum Terjawab'):
                    $tbl = "<a data-toggle='modal' href='#basic' class='btn btn-success btn-jawab'>Jawab</a>";
                else:
                    $tbl = "<form action='komponen/detail' method='post'><input type='hidden' name='id_peserta' value='$peserta->id_peserta'><input type='hidden' name='id_komponen' value='$x->id_komponen'><input type='submit' value='Lengkapi Data' class='btn btn-edit sbold blue'></form>";
                endif;
                $komponen[] = array(
                    $x->id_komponen,
                    $x->komponen,
                    $tbl,
//                    "<a href='komponen/detail?id=$x->id_komponen' class='btn btn-edit sbold blue'> Isi Instrumen </a>",
                    "<span class='label label-" . $jawaban['status'] . "'>" . $jawaban['jawaban'] . "<span>"
                );
            }
        } else {
            $komponen = array();
        }
        $komponen = array('data' => $komponen);
        echo json_encode($komponen);
//        print_r($data_komp);
    }

    public function detail() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            redirect('dashboard');
        }
//        $this->load->helper('directory');
        $data = $this->data;
        $id_komponen = $this->input->post('id_komponen');
        $id_peserta = $this->input->post('id_peserta');
        $data['level_2'] = $id_komponen;
//        $data['level_3'] = 
        $opt['conditions'] = array('id_komponen' => $id_komponen);
        $komponen = $this->get_komponen($id_komponen, $id_peserta);
        $data['komponen'] = $komponen;
        $opt_jawaban['conditions'] = array('id_komponen' => $id_komponen, 'id_peserta' => $id_peserta);
        if (count($this->Jawaban_model->paginate_read($opt_jawaban)) > 0) {
            $jawaban = $this->Jawaban_model->paginate_read($opt_jawaban);
            $data['jawaban'] = $jawaban;
            $opt_data['conditions'] = array('id_jawaban' => $jawaban->id_jawaban);
            $data['data'] = $this->Data_model->paginate($opt_data);
        }
//        $data['id_peserta'] = $this->input->post('id_peserta');
//        $data['komponen'] = $this->get_komponen($id_komponen);
        $this->session->set_flashdata('kat_komp', $komponen->id_kat_komp);
        $data['id_peserta'] = $id_peserta;
        $this->render('komponen_detail_view', $data);
    }

    public function cek_jawaban($id_komp, $id_peserta) {
        $opt['conditions'] = array('id_komponen' => $id_komp, 'id_peserta' => $id_peserta);
        if (count($this->Jawaban_model->paginate_read($opt)) > 0) {
            $ket['jawaban'] = "Terjawab";
            $ket['status'] = 'success';
        } else {
            $ket['jawaban'] = "Belum Terjawab";
            $ket['status'] = 'danger';
        }
        return $ket;
    }

    public function get_komponen($id_komponen, $id_peserta) {
        $opt['conditions'] = array('id_komponen' => $id_komponen);
        $komponen = $this->Komponen_model->paginate_read($opt);
        $this->load->model('Var_komp_model');
        $opt_var['conditions'] = array('id_var_komp' => $komponen->id_var_komp);
        $var_komp = $this->Var_komp_model->paginate_read($opt_var);
        $opt_jawaban['conditions'] = array('id_komponen' => $id_komponen, 'id_peserta' => $id_peserta);
        $jawaban = $this->Jawaban_model->paginate_read($opt_jawaban);
        $form2 = '';
        $status = '';
        $status2 = '';
        if ($var_komp->type == 'checkbox') {
            if (count($jawaban) > 0):
                $status = TRUE;
            else:
                $status = FALSE;
            endif;
            $form = form_checkbox(array(
                'name' => 'jawaban',
                'type' => 'checkbox',
                'class' => $var_komp->class,
                'value' => 'checked',
                'checked' => $status,
                'required' => 'required'
            ));
        } else if ($var_komp->type == 'text') {
            if (count($jawaban) > 0):
                $status = $jawaban->jawaban_1;
            endif;
            $form = form_input(array(
                'name' => 'jawaban',
                'type' => 'text',
                'class' => $var_komp->class,
                'value' => $status,
                'required' => 'required'
            ));
        } else {
            if (count($jawaban) > 0):
                $status = $jawaban->jawaban_1;
            endif;
            $form = form_input(array(
                'name' => 'jawaban',
                'type' => 'text',
                'class' => $var_komp->class,
                'value' => $status,
                'required' => 'required'
            ));
            if (isset($jawaban->jawaban_2)):
                $status2 = $jawaban->jawaban_2;
            endif;
            $form2 = form_input(array(
                'name' => 'jawaban2',
                'type' => 'text',
                'class' => $var_komp->class,
                'value' => $status2
            ));
        }

        $komponen->form = $form;
        $komponen->form2 = $form2;
        $komponen->class = $var_komp->class;
        return $komponen;
    }

    public function get_form() {
        $id_komponen = $this->input->post('id_komponen');
        $opt['conditions'] = array('id_komponen' => $id_komponen);
        $komponen = $this->Komponen_model->paginate_read($opt);
        $opt_var['conditions'] = array('id_var_komp' => $komponen->id_var_komp);
        $var_komp = $this->Var_komp_model->paginate_read($opt_var);
        $type = $var_komp->type;
        if ($type == 'text') {
            echo "<input type='text' name='jawaban_1' class='$var_komp->class'><input type='hidden' name='id_komponen' value='$komponen->id_komponen'>";
        } else if ($type == 'rasio') {
            echo "<input type='hidden' name='id_komponen' value='$komponen->id_komponen'><input type='text' name='jawaban_1' class='$var_komp->class'><span class='input-group-addon'>$komponen->label</span><input type='text' name='jawaban_2' class='$var_komp->class'>";
        } else {
            echo "<input type='hidden' name='id_komponen' value='$komponen->id_komponen'><input type='checkbox' name='jawaban_1' class='$var_komp->class'>";
        }
    }

    public function jawab() {
        $id_komponen = $this->input->post('id_komponen');
        $jawaban_1 = $this->input->post('jawaban_1', true);
        if (isset($_POST['jawaban_2'])):
            $jawaban_2 = $this->input->post('jawaban_2', true);
        else:
            $jawaban_2 = null;
        endif;
        $result = array(
            'iserror' => 101,
            'errmsg' => 'Gagal tambah data !');
        $peserta = $this->Peserta_model->paginate_read(array('conditions' => array('id_user' => $this->session->userdata('id_user'))));
        $Jawaban = new Jawaban_model();
        $Jawaban->id_komponen = $id_komponen;
        $Jawaban->jawaban_1 = $jawaban_1;
        $Jawaban->jawaban_2 = $jawaban_2;
        $Jawaban->id_peserta = $peserta->id_peserta;
        $Jawaban->created_at = Carbon::now()->toDateTimeString();
        if (!$Jawaban->save()) {
            $result = array(
                'iserror' => 101,
                'errmsg' => 'Gagal mengirim data !');
        } else {
            $result = array(
                'iserror' => 100,
                'errmsg' => 'Berhasil mengirim data !');
        }
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        echo json_encode($result);
    }

}

?>